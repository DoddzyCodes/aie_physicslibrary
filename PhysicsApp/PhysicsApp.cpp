#include "PhysicsApp.h"
#include "Gizmos.h"
#include "Input.h"
#include "PhysicsRigidbody.h"
#include <glm/ext.hpp>
#include <glm/glm.hpp>

#include "GameObject.h"
#include "PhysicsAABBShape.h"
#include "PhysicsCollision.h"
#include "PhysicsObject.h"
#include "PhysicsPlaneShape.h"
#include "PhysicsScene.h"
#include "PhysicsSphereShape.h"
#include "PhysicsSpring.h"

using glm::vec3;
using glm::vec4;
using glm::mat4;
using aie::Gizmos;

PhysicsApp::PhysicsApp() {}

PhysicsApp::~PhysicsApp() {}

bool PhysicsApp::startup()
{
    setBackgroundColour(0.25f, 0.25f, 0.25f);

    // initialise gizmo primitive counts
    Gizmos::create(10000, 10000, 10000, 10000);

    // create simple camera transforms
    m_viewMatrix = glm::lookAt(vec3(30, 30, 30), vec3(0), vec3(0, 1, 0));
    m_projectionMatrix =
        glm::perspective(glm::pi<float>() * 0.25f,
                         getWindowWidth() / (float)getWindowHeight(),
                         0.1f,
                         1000.f);

    m_physicsScene = new PhysicsScene();
    m_physicsScene->SetGravity(glm::vec3(0.0f, -9.8f, 0.0f));

    auto floor = CreatePhysicsObject(PhysicsShape::ShapeType::Plane);
    m_worldObjects.push_back(floor);

    auto aabb = CreatePhysicsObject(PhysicsShape::ShapeType::AABB);
    aabb->GetPhysicsObject()->SetRigidbody(new PhysicsRigidbody(2.0f));
    aabb->GetPhysicsObject()->SetPosition({0.0f, 15.0f, 0.0f});
    m_worldObjects.push_back(aabb);

    aabb = CreatePhysicsObject(PhysicsShape::ShapeType::AABB);
    aabb->GetPhysicsObject()->SetRigidbody(new PhysicsRigidbody(1.0f));
    aabb->GetPhysicsObject()->SetPosition({0.0f, 10.0f, 0.0f});
    m_worldObjects.push_back(aabb);

    for (auto& gameObject : m_worldObjects)
    {
        auto* physicsObject = gameObject->GetPhysicsObject();
        if (physicsObject)
            m_physicsScene->Add(physicsObject);
    }

    return true;
}

void PhysicsApp::shutdown()
{
    for (auto& gameObject : m_worldObjects)
    {
        delete gameObject;
    }
    delete m_physicsScene;

    Gizmos::destroy();
}

void PhysicsApp::update(float deltaTime)
{
    // wipe the gizmos clean for this frame
    Gizmos::clear();

    // draw a simple grid with gizmos
    vec4 white(1);
    vec4 black(0, 0, 0, 1);
    for (int i = 0; i < 21; ++i)
    {
        Gizmos::addLine(vec3(-10 + i, 0, 10),
                        vec3(-10 + i, 0, -10),
                        i == 10 ? white : black);
        Gizmos::addLine(vec3(10, 0, -10 + i),
                        vec3(-10, 0, -10 + i),
                        i == 10 ? white : black);
    }

    // add a transform so that we can see the axis
    Gizmos::addTransform(mat4(1));

    // Apply a small force to our demo objects physics
    // object - we should see it start to move down the
    //'x' axis
    // m_demoGameObject->GetPhysicsObject()->AddForce(glm::vec3(1, 0, 0));

    m_physicsScene->Update(deltaTime);

    // quit if we press escape
    aie::Input* input = aie::Input::getInstance();

    if (input->isKeyDown(aie::INPUT_KEY_SPACE))
    {
        m_ball->GetPhysicsObject()->GetRigidbody()->AddForce(
            glm::vec3(1, 0, 0) * 100.0f);
    }

    if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
        quit();
}

void PhysicsApp::draw()
{
    // wipe the screen to the background colour
    clearScreen();

    // update perspective in case window resized
    m_projectionMatrix =
        glm::perspective(glm::pi<float>() * 0.25f,
                         getWindowWidth() / (float)getWindowHeight(),
                         0.1f,
                         10000.f);

    for (auto& gameObject : m_worldObjects)
    {
        gameObject->DebugPhysicsRender();
    }
    Gizmos::draw(m_projectionMatrix * m_viewMatrix);
}

GameObject* PhysicsApp::CreatePhysicsObject(PhysicsShape::ShapeType type)
{
    auto go = new GameObject();
    PhysicsShape* shape;
    switch (type)
    {
    case PhysicsShape::ShapeType::AABB:
        shape = new PhysicsAABBShape(glm::vec3(1.0f, 1.0f, 1.0f));
        break;

    case PhysicsShape::ShapeType::Sphere:
        shape = new PhysicsSphereShape(1.0f);
        break;

    case PhysicsShape::ShapeType::Plane:
        shape = new PhysicsPlaneShape(glm::vec3(0, 1, 0), 0.0f);
        break;

    case PhysicsShape::ShapeType::NUM_SHAPES:
        break;
    }

    go->SetPhysicsObject(new PhysicsObject());
    go->GetPhysicsObject()->AddShape(shape);

    return go;
}