#pragma once

#include "Application.h"
#include <PhysicsShape.h>
#include <glm/mat4x4.hpp>
#include <vector>
class PhysicsScene;
class GameObject;

class PhysicsApp : public aie::Application
{
  public:
    PhysicsApp();
    virtual ~PhysicsApp();

    virtual bool startup();
    virtual void shutdown();

    virtual void update(float deltaTime);
    virtual void draw();

  protected:
    GameObject* CreatePhysicsObject(PhysicsShape::ShapeType type);

    PhysicsScene* m_physicsScene;

    std::vector<GameObject*> m_worldObjects;

    glm::mat4 m_viewMatrix;
    glm::mat4 m_projectionMatrix;
    GameObject* m_ball;
};