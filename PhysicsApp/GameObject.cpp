#include "GameObject.h"
#include "Gizmos.h"
#include "PhysicsObject.h"
#include <glm/glm.hpp>

#include "PhysicsAABBShape.h"
#include "PhysicsPlaneShape.h"
#include "PhysicsSphereShape.h"

GameObject::GameObject() : m_physicsObject(nullptr) {}

void GameObject::DebugPhysicsRender()
{
    if (m_physicsObject != nullptr)
    {
        // Only do a debug render if this
        // object has a physics object attached
        glm::vec3 position = m_physicsObject->GetPosition();

        PhysicsShape* pShape = m_physicsObject->GetShape();
        if (pShape == nullptr)
            return;

        switch (pShape->GetType())
        {

        default:
            break;
        case PhysicsShape::ShapeType::Plane:
        {
            PhysicsPlaneShape* pPlane = (PhysicsPlaneShape*)pShape;
            glm::vec3 worldUp         = glm::vec3(0, 1, 0);
            glm::vec3 worldForward    = glm::vec3(0, 0, 1);
            glm::vec3 right;
            glm::vec3 forward;
            glm::vec3 up;

            float fDot = glm::dot(worldUp, pPlane->GetNormal());
            if (glm::abs(fDot) < 0.99f)
            {
                right = pPlane->GetNormal();
                // Gets the perpendicular vector between the right and worlds up
                // We will use this as our forward vector (if you do the cross
                // in the
                // other direction,
                // things will be backwards
                forward = glm::cross(right, worldUp);
                // We  can now do the cross product between the forward and
                // right
                // vectors to calculate
                // the actual up vector - this will be perpendicular to both the
                // actual
                // forward and actual
                // right vectors
                up = glm::cross(forward, right);
            }
            else
            {
                right = pPlane->GetNormal();
                // Gets the perpendicular vector between the world forward and
                // our right
                // We will use this as our up vector (if you do the cross in the
                // other
                // direction,
                // things will be backwards
                up = glm::cross(worldForward, right);
                // We  can now do the cross product between the right and up
                // vectors to
                // calculate
                // the actual up vector - this will be perpendicular to both the
                // actual
                // forward and actual
                // right vectors
                forward = glm::cross(right, up);
            }

            glm::mat4 transform(1);
            transform[0] = glm::vec4(right, 0);
            transform[1] = glm::vec4(up, 0);
            transform[2] = glm::vec4(forward, 0);
            transform[3] = glm::vec4(
                pPlane->GetDistanceFromOrigin() * pPlane->GetNormal(), 1);

            aie::Gizmos::addAABBFilled(glm::vec3(0, 0, 0),
                                       glm::vec3(0.0001f, 1000.0f, 1000.0f),
                                       glm::vec4(1, 0, 1, 1),
                                       &transform);

            break;
        }
        case PhysicsShape::ShapeType::AABB:
        {
            PhysicsAABBShape* pAABB = (PhysicsAABBShape*)pShape;

            aie::Gizmos::addAABBFilled(
                position, pAABB->GetExtents(), glm::vec4(0, 1, 0, 1));
            break;
        }
        case PhysicsShape::ShapeType::Sphere:
        {
            PhysicsSphereShape* pSphere = (PhysicsSphereShape*)pShape;
            aie::Gizmos::addSphere(
                position, pSphere->GetRadius(), 15, 15, glm::vec4(1, 0, 0, 1));
            break;
        }
        }
    }
}

void GameObject::SetPhysicsObject(PhysicsObject* physicsObject)
{
    m_physicsObject = physicsObject;
}

PhysicsObject* GameObject::GetPhysicsObject() const { return m_physicsObject; }
