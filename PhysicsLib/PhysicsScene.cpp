#include "PhysicsScene.h"
#include "PhysicsCollision.h"
#include "PhysicsObject.h"
#include "PhysicsRigidbody.h"
#include "PhysicsSpring.h"
PhysicsScene::PhysicsScene() : m_gravity(glm::vec3(0, 0, 0)) {}

PhysicsScene::~PhysicsScene()
{
    for (PhysicsObject* object : m_physicsObjects)
    {
        delete object;
    }
}

void PhysicsScene::Add(PhysicsObject* newObject)
{
    // Adds the given physics object to our
    // set of tracked physics objects.
    m_physicsObjects.insert(newObject);
}

void PhysicsScene::Remove(PhysicsObject* objectToRemove)
{
    // Search for the object in the set
    auto it = m_physicsObjects.find(objectToRemove);
    // if found, delete and remove it.
    if (it != m_physicsObjects.end())
    {
        delete *it;
        m_physicsObjects.erase(it);
    }
}

void PhysicsScene::Add(PhysicsSpring* newSpring)
{
    m_physicsSprings.insert(newSpring);
}

void PhysicsScene::Remove(PhysicsSpring* springToRemove)
{
    auto it = m_physicsSprings.find(springToRemove);
    if (it != m_physicsSprings.end())
    {
        delete *it;
        m_physicsSprings.erase(it);
    }
}

void PhysicsScene::Update(float deltaTime)
{
    for (PhysicsSpring* spring : m_physicsSprings)
    {
        spring->Update(deltaTime);
    }

    for (PhysicsObject* object : m_physicsObjects)
    {
        if (!object->IsStatic())
        {
            object->GetRigidbody()->AddAcceleration(m_gravity);
            object->Update(deltaTime);
        }
    }

    PhysicsCollision::CollisionInfo collisionInfo;
    for (auto objIteratorOne = m_physicsObjects.begin();
         objIteratorOne != m_physicsObjects.end();
         objIteratorOne++)
    {
        for (auto objIteratorTwo = std::next(objIteratorOne);
             objIteratorTwo != m_physicsObjects.end();
             objIteratorTwo++)
        {
            bool wasCollision = PhysicsCollision::CheckCollision(
                *objIteratorOne, *objIteratorTwo, collisionInfo);
            if (wasCollision)
            {
                // Handle the collision!
                PhysicsCollision::ResolveCollision(
                    *objIteratorOne, *objIteratorTwo, collisionInfo);
            }
        }
    }
}

void PhysicsScene::SetGravity(const glm::vec3& gravity) { m_gravity = gravity; }

glm::vec3 PhysicsScene::GetGravity() const { return m_gravity; }
