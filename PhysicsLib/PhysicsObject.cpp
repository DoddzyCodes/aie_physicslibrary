#include "PhysicsObject.h"
#include "PhysicsRigidbody.h"
#include "PhysicsShape.h"

PhysicsObject::PhysicsObject() : m_position(0.0f, 0.0f, 0.0f), m_shape(nullptr)
{
}

PhysicsObject::~PhysicsObject() {}

void PhysicsObject::SetPosition(const glm::vec3& newPosition)
{
    m_position = newPosition;
}

glm::vec3 PhysicsObject::GetPosition() const { return m_position; }

void PhysicsObject::Update(float deltaTime)
{
    if (m_rigidbody)
    {
        m_rigidbody->Update(deltaTime);
        m_position += m_rigidbody->GetVelocity() * deltaTime;
    }
}

void PhysicsObject::AddShape(PhysicsShape* shape) { m_shape = shape; }

void PhysicsObject::RemoveShape()
{
    delete m_shape;
    m_shape = nullptr;
}

PhysicsShape* PhysicsObject::GetShape() const { return m_shape; }
