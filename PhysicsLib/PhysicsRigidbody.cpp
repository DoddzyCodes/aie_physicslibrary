#include "PhysicsRigidbody.h"

void PhysicsRigidbody::SetMass(float mass) { m_mass = mass; }

float PhysicsRigidbody::GetMass() const { return m_mass; }

void PhysicsRigidbody::SetVelocity(const glm::vec3& newVelocity)
{
    m_velocity = newVelocity;
}

glm::vec3 PhysicsRigidbody::GetVelocity() const { return m_velocity; }

void PhysicsRigidbody::AddForce(const glm::vec3& force) { m_force += force; }

void PhysicsRigidbody::AddAcceleration(const glm::vec3& acceleration)
{
    m_acceleration = acceleration;
}

void PhysicsRigidbody::AddVelocity(const glm::vec3& velocity)
{
    m_velocity = velocity;
}

void PhysicsRigidbody::Update(float deltaTime)
{

    m_acceleration += m_force / m_mass;
    m_velocity = m_velocity + m_acceleration * deltaTime;

    m_force        = glm::vec3(0.0f, 0.0f, 0.0f);
    m_acceleration = glm::vec3(0.0f, 0.0f, 0.0f);
}