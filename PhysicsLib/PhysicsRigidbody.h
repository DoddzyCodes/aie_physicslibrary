#pragma once
#include <glm/glm.hpp>
class PhysicsRigidbody
{
  public:
    PhysicsRigidbody() = default;
    PhysicsRigidbody(float mass) : m_mass(mass) {}

    void Update(float deltaTime);

    void SetMass(const float newMass);
    float GetMass() const;

    void SetVelocity(const glm::vec3& newVelocity);
    glm::vec3 GetVelocity() const;

    void AddForce(const glm::vec3& force);
    void AddAcceleration(const glm::vec3& acceleration);
    void AddVelocity(const glm::vec3& velocity);

  private:
    float m_mass             = 1.0f;
    glm::vec3 m_velocity     = {0.0f, 0.0f, 0.0f};
    glm::vec3 m_force        = {0.0f, 0.0f, 0.0f};
    glm::vec3 m_acceleration = {0.0f, 0.0f, 0.0f};
};