#pragma once
#include <glm/glm.hpp>

class PhysicsShape;

class PhysicsObject
{
  public:
    PhysicsObject();
    ~PhysicsObject();

    void SetPosition(const glm::vec3& newPosition);
    glm::vec3 GetPosition() const;

    void Update(float deltaTime);

    void AddShape(PhysicsShape* shape);
    void RemoveShape();

    PhysicsShape* GetShape() const;

    void SetRigidbody(class PhysicsRigidbody* rigidbody)
    {
        m_rigidbody = rigidbody;
    }

    class PhysicsRigidbody* GetRigidbody() const { return m_rigidbody; }

    bool IsStatic() const { return m_rigidbody == nullptr; }

  private:
    class PhysicsRigidbody* m_rigidbody = nullptr;
    class PhysicsShape* m_shape         = nullptr;
    glm::vec3 m_position                = {0.0f, 0.0f, 0.0f};
};