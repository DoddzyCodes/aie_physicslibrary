#include "PhysicsCollision.h"
#include "PhysicsAABBShape.h"
#include "PhysicsObject.h"
#include "PhysicsPlaneShape.h"
#include "PhysicsRigidbody.h"
#include "PhysicsShape.h"
#include "PhysicsSphereShape.h"
bool PhysicsCollision::CheckCollision(const PhysicsObject* obj1,
                                      const PhysicsObject* obj2,
                                      CollisionInfo& collisionInfo)
{
    // Create a function pointer type for our collision functions
    typedef bool (*collisionFnc)(const PhysicsObject* obj1,
                                 const PhysicsObject* obj2,
                                 CollisionInfo& collisionInfo);

    // Create an array with all of our collision functions
    // These are ordered based on the "ShapeType" enum order
    static collisionFnc collisionFunctionArray[] = {nullptr,
                                                    CheckPlaneAABBCollision,
                                                    CheckPlaneSphereCollision,
                                                    CheckAABBPlaneCollision,
                                                    CheckAABBAABBCollision,
                                                    CheckAABBSphereCollision,
                                                    CheckSpherePlaneCollision,
                                                    CheckSphereAABBCollision,
                                                    CheckSphereSphereCollision};

    //'hash' the two shapeIDs to create a unique index
    auto index = ((int)(obj1->GetShape()->GetType()) *
                  (int)PhysicsShape::ShapeType::NUM_SHAPES) +
                 (int)(obj2->GetShape()->GetType());

    // If we have a collision function for this combination of shapes, call the
    // collision function
    if (collisionFunctionArray[index] != nullptr)
    {
        return collisionFunctionArray[index](obj1, obj2, collisionInfo);
    }
    // otherwise, return false
    return false;
}

bool PhysicsCollision::CheckSpherePlaneCollision(const PhysicsObject* obj1,
                                                 const PhysicsObject* obj2,
                                                 CollisionInfo& collisionInfo)
{
    PhysicsSphereShape* pSphere = (PhysicsSphereShape*)obj1->GetShape();
    PhysicsPlaneShape* pPlane   = (PhysicsPlaneShape*)obj2->GetShape();

    float distanceFromPlane =
        (glm::dot(obj1->GetPosition(), pPlane->GetNormal())) -
        pPlane->GetDistanceFromOrigin();
    if (distanceFromPlane < pSphere->GetRadius())
    {
        // We have a collision!
        collisionInfo.normal = -pPlane->GetNormal();
        collisionInfo.interceptDistance =
            pSphere->GetRadius() - distanceFromPlane;

        collisionInfo.wasCollision = true;
    }
    else
    {
        // No Collision!
        collisionInfo.wasCollision = false;
    }

    return collisionInfo.wasCollision;
}

bool PhysicsCollision::CheckPlaneSphereCollision(const PhysicsObject* obj1,
                                                 const PhysicsObject* obj2,
                                                 CollisionInfo& collisionInfo)
{
    bool wasCollision = CheckSpherePlaneCollision(obj2, obj1, collisionInfo);
    if (wasCollision)
    {
        // Because we did the calculation backwards, we need to also flip the
        // collision normal
        collisionInfo.normal = -collisionInfo.normal;
    }
    return collisionInfo.wasCollision;
}

bool PhysicsCollision::CheckPlaneAABBCollision(const PhysicsObject* obj1,
                                               const PhysicsObject* obj2,
                                               CollisionInfo& collisionInfo)
{
    PhysicsPlaneShape* pPlane = (PhysicsPlaneShape*)obj1->GetShape();
    PhysicsAABBShape* pAABB   = (PhysicsAABBShape*)obj2->GetShape();

    glm::vec3 aabbMin = obj2->GetPosition() - pAABB->GetExtents();
    glm::vec3 aabbMax = obj2->GetPosition() + pAABB->GetExtents();

    float distanceMin = glm::dot(pPlane->GetNormal(), aabbMin) -
                        pPlane->GetDistanceFromOrigin();
    float distanceMax = glm::dot(pPlane->GetNormal(), aabbMax) -
                        pPlane->GetDistanceFromOrigin();

    float smallestDistance =
        (distanceMin < distanceMax) ? distanceMin : distanceMax;

    if (smallestDistance < 0)
    {
        collisionInfo.interceptDistance = -smallestDistance;
        collisionInfo.normal            = pPlane->GetNormal();
        collisionInfo.wasCollision      = true;
    }
    else
    {
        collisionInfo.wasCollision = false;
    }

    return collisionInfo.wasCollision;
}

bool PhysicsCollision::CheckAABBPlaneCollision(const PhysicsObject* obj1,
                                               const PhysicsObject* obj2,
                                               CollisionInfo& collisionInfo)
{
    bool wasCollision = CheckPlaneAABBCollision(obj2, obj1, collisionInfo);
    if (wasCollision)
        collisionInfo.normal = -collisionInfo.normal;
    return collisionInfo.wasCollision;
}
bool PhysicsCollision::CheckAABBSphereCollision(const PhysicsObject* obj1,
                                                const PhysicsObject* obj2,
                                                CollisionInfo& collisionInfo)
{
    auto aabb   = (PhysicsAABBShape*)obj1->GetShape();
    auto sphere = (PhysicsSphereShape*)obj2->GetShape();

    auto offset = obj2->GetPosition() - obj1->GetPosition();

    auto extents = aabb->GetExtents();
    auto edge    = glm::vec3(0);
    edge.x =
        glm::clamp(glm::dot(offset, glm::vec3(1, 0, 0)), -extents.x, extents.x);
    edge.y =
        glm::clamp(glm::dot(offset, glm::vec3(0, 1, 0)), -extents.y, extents.y);
    edge.z =
        glm::clamp(glm::dot(offset, glm::vec3(0, 0, 1)), -extents.z, extents.z);

    auto aabbClosestPoint = obj1->GetPosition() + edge;

    offset      = obj2->GetPosition() - aabbClosestPoint;
    auto length = glm::length(offset);
    if (length < sphere->GetRadius() && length > 0)
    {
        collisionInfo.wasCollision      = true;
        collisionInfo.normal            = glm::normalize(offset);
        collisionInfo.interceptDistance = sphere->GetRadius() - length;
    }
    else
    {
        collisionInfo.wasCollision = false;
    }
    return collisionInfo.wasCollision;
}

bool PhysicsCollision::CheckSphereAABBCollision(const PhysicsObject* obj1,
                                                const PhysicsObject* obj2,
                                                CollisionInfo& collisionInfo)
{
    bool wasCollision = CheckAABBSphereCollision(obj2, obj1, collisionInfo);
    if (wasCollision)
        collisionInfo.normal = -collisionInfo.normal;
    return collisionInfo.wasCollision;
}

bool PhysicsCollision::CheckAABBAABBCollision(const PhysicsObject* obj1,
                                              const PhysicsObject* obj2,
                                              CollisionInfo& collisionInfo)
{
    auto aabb1 = (PhysicsAABBShape*)obj1->GetShape();
    auto aabb2 = (PhysicsAABBShape*)obj2->GetShape();

    auto min1 = obj1->GetPosition() - aabb1->GetExtents();
    auto min2 = obj2->GetPosition() - aabb2->GetExtents();

    auto max1 = obj1->GetPosition() + aabb1->GetExtents();
    auto max2 = obj2->GetPosition() + aabb2->GetExtents();

    if (max1.x < min2.x || min1.x > max2.x || max1.y < min2.y ||
        min1.y > max2.y || max1.z > max2.z || min1.z > max2.z)
    {
        collisionInfo.wasCollision = false;
        return false;
    }

    auto offset1 = max1 - min2;
    auto offset2 = min1 - max2;

    auto smallestOffset = glm::vec3(0);
    smallestOffset.x =
        (fabs(offset1.x) < fabs(offset2.x)) ? offset1.x : offset2.x;
    smallestOffset.y =
        (fabs(offset1.y) < fabs(offset2.y)) ? offset1.y : offset2.y;
    smallestOffset.z =
        (fabs(offset1.z) < fabs(offset2.z)) ? offset1.z : offset2.z;

    auto normal    = glm::vec3(1, 0, 0);
    auto intercept = smallestOffset.x;
    if (fabs(smallestOffset.y) < fabs(intercept))
    {
        normal    = glm::vec3(0, 1, 0);
        intercept = smallestOffset.y;
    }

    if (fabs(smallestOffset.z) < fabs(intercept))
    {
        normal    = glm::vec3(0, 0, 1);
        intercept = smallestOffset.z;
    }

    collisionInfo.wasCollision      = true;
    collisionInfo.normal            = normal;
    collisionInfo.interceptDistance = intercept;

    return true;
}

bool PhysicsCollision::CheckSphereSphereCollision(const PhysicsObject* obj1,
                                                  const PhysicsObject* obj2,
                                                  CollisionInfo& collisionInfo)
{
    PhysicsSphereShape* pSphere1 = (PhysicsSphereShape*)obj1->GetShape();
    PhysicsSphereShape* pSphere2 = (PhysicsSphereShape*)obj2->GetShape();

    float minDistance = pSphere1->GetRadius() + pSphere2->GetRadius();
    glm::vec3 offset  = obj2->GetPosition() - obj1->GetPosition();
    float length      = glm::length(offset);
    if (length < minDistance)
    {
        collisionInfo.wasCollision      = true;
        collisionInfo.normal            = glm::normalize(offset);
        collisionInfo.interceptDistance = minDistance - length;
    }
    else
    {
        collisionInfo.wasCollision = false;
    }

    return collisionInfo.wasCollision;
}

void PhysicsCollision::ResolveCollision(PhysicsObject* obj1,
                                        PhysicsObject* obj2,
                                        CollisionInfo& collisionInfo)
{
    if (collisionInfo.wasCollision)
    {
        HandleSeperation(obj1, obj2, collisionInfo);
        HandleVelocityChange(obj1, obj2, collisionInfo);
    }
}

void PhysicsCollision::HandleSeperation(PhysicsObject* obj1,
                                        PhysicsObject* obj2,
                                        CollisionInfo& collisionInfo)
{
    assert(!obj1->IsStatic() ||
           !obj2->IsStatic() && "At least one object must be static");
    float obj1Offset = 0.0f;
    float obj2Offset = 0.0f;

    if (!obj1->IsStatic() && !obj2->IsStatic())
    {
        float totalSystemMass =
            obj1->GetRigidbody()->GetMass() + obj2->GetRigidbody()->GetMass();

        obj1Offset = collisionInfo.interceptDistance *
                     (obj2->GetRigidbody()->GetMass() / totalSystemMass);
        obj2Offset = collisionInfo.interceptDistance *
                     (obj1->GetRigidbody()->GetMass() / totalSystemMass);
    }
    else
    {
        obj1Offset = (!obj1->IsStatic()) ? 1.0f : 0.0f;
        obj2Offset = (!obj2->IsStatic()) ? 1.0f : 0.0f;
    }
    glm::vec3 obj1NewPosition =
        obj1->GetPosition() - (obj1Offset * collisionInfo.normal);
    glm::vec3 obj2NewPosition =
        obj2->GetPosition() + (obj2Offset * collisionInfo.normal);

    obj1->SetPosition(obj1NewPosition);
    obj2->SetPosition(obj2NewPosition);
}

void PhysicsCollision::HandleVelocityChange(PhysicsObject* obj1,
                                            PhysicsObject* obj2,
                                            CollisionInfo& collisionInfo)
{
    float e = 0.7f;
    glm::vec3 relativeVelocity(0);
    float jMass = 0.0f;

    if (!obj2->IsStatic())
    {
        relativeVelocity = obj2->GetRigidbody()->GetVelocity();
        jMass += 1.0f / obj2->GetRigidbody()->GetMass();
    }
    if (!obj1->IsStatic())
    {

        relativeVelocity -= obj1->GetRigidbody()->GetVelocity();
        jMass += 1.0f / obj1->GetRigidbody()->GetMass();
    }

    float jTop = -(1 + e) * glm::dot(relativeVelocity, collisionInfo.normal);
    float jBottom =
        glm::dot(collisionInfo.normal, collisionInfo.normal) * jMass;
    float j = jTop / jBottom;

    if (!obj1->IsStatic())
    {
        glm::vec3 obj1NewVelocity =
            obj1->GetRigidbody()->GetVelocity() -
            ((j / obj1->GetRigidbody()->GetMass()) * collisionInfo.normal);
        obj1->GetRigidbody()->SetVelocity(obj1NewVelocity);
    }
    if (!obj2->IsStatic())
    {

        glm::vec3 obj2NewVelocity =
            obj2->GetRigidbody()->GetVelocity() +
            ((j / obj2->GetRigidbody()->GetMass()) * collisionInfo.normal);

        obj2->GetRigidbody()->SetVelocity(obj2NewVelocity);
    }
}
