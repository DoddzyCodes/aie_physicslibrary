#pragma once
#include <glm/glm.hpp>
#include <set>

class PhysicsObject;
class PhysicsSpring;

class PhysicsScene
{
  public:
    PhysicsScene();
    ~PhysicsScene();

    void Add(PhysicsObject* newObject);
    void Remove(PhysicsObject* objectToRemove);

    void Add(PhysicsSpring* newSpring);
    void Remove(PhysicsSpring* springToRemove);

    void Update(float deltaTime);

    void SetGravity(const glm::vec3& gravity);
    glm::vec3 GetGravity() const;

  private:
    std::set<PhysicsObject*> m_physicsObjects;
    std::set<PhysicsSpring*> m_physicsSprings;

    glm::vec3 m_gravity;
};
