#pragma once
#include "PhysicsObject.h"

class PhysicsSpring
{
  public:
    PhysicsSpring() = delete;
    PhysicsSpring(PhysicsObject& first, PhysicsObject& second);
    ~PhysicsSpring();

    void Update(float deltaTIme);

    void SetWantedDisplacement(float displacement)
    {
        m_wantedDisplacement = displacement;
    }

    void SetStiffness(float stiffness) { m_stiffness = stiffness; }
    void SetDampening(float dampening) { m_dampening = dampening; }

  private:
    PhysicsObject& m_first;
    PhysicsObject& m_second;

    float m_wantedDisplacement;
    float m_stiffness;
    float m_dampening;
};