#include "PhysicsSpring.h"
#include "PhysicsRigidbody.h"
#include <cstdio>
#include <glm/glm.hpp>
PhysicsSpring::PhysicsSpring(PhysicsObject& first, PhysicsObject& second)
    : m_first(first), m_second(second), m_stiffness(1.0f), m_dampening(1.0f)
{
    auto offset          = m_second.GetPosition() - m_first.GetPosition();
    m_wantedDisplacement = glm::length(offset);
}

PhysicsSpring::~PhysicsSpring() {}

void PhysicsSpring::Update(float deltaTime)
{

    auto offset           = m_second.GetPosition() - m_first.GetPosition();
    auto relativeVelocity = glm::vec3(0);

    if (!m_second.IsStatic())
        relativeVelocity = m_second.GetRigidbody()->GetVelocity();

    if (!m_first.IsStatic())
        relativeVelocity -= m_first.GetRigidbody()->GetVelocity();

    auto displacement = glm::length(offset);

    auto direction = glm::normalize(offset);

    auto X = m_wantedDisplacement - displacement;

    auto springForce = (m_stiffness * X) * direction;
    auto dampening   = (m_dampening * relativeVelocity);

    auto actualForce = springForce - dampening;

    if (!m_first.IsStatic())
        m_first.GetRigidbody()->AddForce(-actualForce);
    if (!m_second.IsStatic())
        m_second.GetRigidbody()->AddForce(actualForce);
}